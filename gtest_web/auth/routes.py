import flask
import flask_login

from gtest_web.auth.shared import LoginError, RegisterError, login_user, register_user, load_dynamodb_tables
from gtest_web.auth.forms import LoginForm, RegisterForm

auth_blueprint = flask.Blueprint("auth", __name__, template_folder="templates")
auth_blueprint.before_app_request(load_dynamodb_tables)

@auth_blueprint.route("/login", methods=("GET", "POST"))
def login():
    form = LoginForm()
    if form.validate_on_submit():
        try:
            return login_user(form.data.get("login"), form.data.get("password"))
        except LoginError as exception:
            form.password.errors += exception.args
    return flask.render_template("auth/login.html", form=form, anon=True)

@auth_blueprint.route("/register", methods=("GET", "POST"))
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        try:
            return register_user(form.data["login"], form.data["password"])
        except RegisterError as exception:
            form.login.errors += exception.args
    return flask.render_template("auth/register.html", form=form, anon=True)

@auth_blueprint.route("/logout", methods=("GET",))
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return flask.redirect(flask.url_for("auth.login"))
