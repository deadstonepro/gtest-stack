from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, EqualTo, ValidationError

class LoginForm(FlaskForm):
    login = StringField("login", validators=[InputRequired()])
    password = PasswordField("password", validators=[InputRequired()])

class RegisterForm(FlaskForm):
    login = StringField("Login", validators=[InputRequired()])
    password = PasswordField("Password", validators=[
        InputRequired(),
        EqualTo("confirmation", message="Passwords must match")])
    confirmation = PasswordField("Confirmation", validators=[InputRequired()])
