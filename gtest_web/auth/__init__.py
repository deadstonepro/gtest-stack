from flask_login import LoginManager

from gtest_web.auth.shared import load_user
from gtest_web.auth.routes import auth_blueprint

def create_login_manager():
    login_manager = LoginManager()
    login_manager.user_loader(load_user)
    login_manager.login_view = "auth.login"
    return login_manager
