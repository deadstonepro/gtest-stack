import uuid
from typing import Optional
from urllib.parse import urlparse, urljoin

import boto3
import flask
import flask_login
from boto3.dynamodb.conditions import Key
from werkzeug.security import generate_password_hash, check_password_hash

from shared import data

def load_user(user_id: str):
    table = flask.g.user_table
    expression = Key("user_id").eq(user_id)
    response = table.get_item(Key={"user_id": user_id})
    if "Item" not in response.keys():
        return None
    return data.UserSchema().load(response.get("Item"))

def get_user_by_login(login: str) -> Optional[data.User]:
    table = flask.g.user_table
    expression = Key("login").eq(login)
    response = table.query(IndexName="login-index", Limit=1, KeyConditionExpression=expression)
    if not response.get("Items"):
        return None
    return data.UserSchema().load(response["Items"][0])

def is_safe_url(target):
    ref_url = urlparse(flask.request.host_url)
    test_url = urlparse(urljoin(flask.request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def get_index_url() -> Optional[str]:
    index_view = flask.current_app.config.get("INDEX_VIEW")
    if not index_view:
        return None
    return flask.url_for(index_view)


class RegisterError(Exception):
    pass

def register_user(login: str, password: str):
    existing_login_message = "Login must be unique"
    user_id = str(uuid.uuid4())
    if get_user_by_login(login):
        raise RegisterError(existing_login_message)
    hashed_password = generate_password_hash(password)
    user = data.User(user_id, login, hashed_password)
    user_item = data.UserSchema().dump(user)
    flask.g.user_table.put_item(Item=user_item)
    return flask.redirect(flask.url_for("auth.login"))

class LoginError(Exception):
    pass

def login_user(login: str, password: str):
    bad_creds_message = "There is no user with such login and password"
    user = get_user_by_login(login)
    if not user:
        raise LoginError(bad_creds_message)
    if not check_password_hash(user.hashed_password, password):
        raise LoginError(bad_creds_message)
    flask_login.login_user(user)
    next_url = flask.request.args.get('next')
    if not is_safe_url(next_url):
        return flask.abort(400)
    return flask.redirect(next_url or get_index_url())

def load_dynamodb_tables():
    dynamodb = boto3.resource("dynamodb")
    user_table_name = flask.current_app.config["USER_TABLE_NAME"]
    flask.g.user_table = dynamodb.Table(user_table_name)
