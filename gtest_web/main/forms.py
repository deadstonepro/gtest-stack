from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SelectField
from wtforms.validators import InputRequired

class SubmitForm(FlaskForm):
    problem_id = StringField("Problem", validators=[InputRequired()])
    language = SelectField("Language", choices=[("python3", "python3")], validators=[InputRequired()])
    source_code = TextAreaField("Source Code", validators=[InputRequired()])
