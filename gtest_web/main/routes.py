import json
import uuid
import datetime

import boto3
import flask
import flask_login

from shared import data
from shared.problems import list_problem_ids
from gtest_web.main.forms import SubmitForm

main_blueprint = flask.Blueprint("main", __name__, template_folder="templates")

@main_blueprint.before_app_request
def load_dynamodb_tables():
    dynamodb = boto3.resource("dynamodb")
    flask.g.report_table = dynamodb.Table(flask.current_app.config["REPORT_TABLE_NAME"])

@main_blueprint.route("/", methods=("GET",))
def index():
    index_url = flask.url_for(flask.current_app.config["INDEX_VIEW"])
    return flask.redirect(index_url)

@main_blueprint.route("/problems", methods=("GET",))
@flask_login.login_required
def problems():
    problem_ids = list_problem_ids(flask.current_app.config["PROBLEM_BUCKET"])
    return flask.render_template("main/problems.html", problem_ids=problem_ids)

@main_blueprint.route("/submit/<problem_id>", methods=("GET", "POST"))
@flask_login.login_required
def submit(problem_id):
    form = SubmitForm()
    form.problem_id.data = problem_id
    if form.validate_on_submit():
        solution_id=str(uuid.uuid4())
        solution = data.Solution(
            solution_id=solution_id,
            problem_id=form.problem_id.data,
            user_id=flask_login.current_user.user_id,
            submitted=datetime.datetime.now(),
            language=form.language.data,
            source_code=form.source_code.data)
        solution_item = data.SolutionSchema().dump(solution)
        queue = boto3.resource("sqs").Queue(flask.current_app.config["SOLUTION_QUEUE_URL"])
        queue.send_message(
            MessageBody=json.dumps(solution_item),
            MessageGroupId="solution",
            MessageDeduplicationId=solution_id)
        return flask.redirect(flask.url_for("main.reports"))
    return flask.render_template("main/submit.html", form=form)

@main_blueprint.route("/reports/<solution_id>", methods=("GET",))
@flask_login.login_required
def report(solution_id):
    response = flask.g.report_table.get_item(Key={"solution_id": solution_id})
    if report_item := response.get("Item"):
        _report = data.ReportSchema().load(report_item)
        if _report.user_id != flask_login.current_user.user_id:
            flask.abort(400)
        return flask.render_template("main/report.html", report=_report)
    return flask.abort(404)

@main_blueprint.route("/reports", methods=("GET",))
@flask_login.login_required
def reports():
    user_id = flask_login.current_user.user_id
    params = {
        "Limit": 20,
        "IndexName": "user_id-index",
        "KeyConditionExpression":  boto3.dynamodb.conditions.Key("user_id").eq(user_id)
    }
    response = flask.g.report_table.query(**params)
    items = response.get("Items") or []
    _reports = [data.ReportSchema().load(i) for i in items]
    return flask.render_template("main/reports.html", reports=_reports)
