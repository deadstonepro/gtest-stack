import os
import logging
import pathlib

import flask
import config
from apig_wsgi import make_lambda_handler

from gtest_web.main import main_blueprint
from gtest_web.auth import auth_blueprint, create_login_manager

def configure(app: flask.Flask):
    config_file = os.environ.get("GTEST_CONF", "gtest.cfg")
    config_dict = config.Config(config_file).as_dict()
    app.config.from_mapping(config_dict)
    app.config.from_mapping(config_dict["gtest_web"])

def create_app():
    logging.basicConfig(level=logging.INFO)
    app = flask.Flask(__name__)
    configure(app)
    app.register_blueprint(main_blueprint, url_prefix="/")
    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    login_manager = create_login_manager()
    login_manager.init_app(app)
    return app

app = create_app()
handler = make_lambda_handler(app.wsgi_app)
