import io
import json
from typing import List

import boto3
import botocore

from .data import Problem, TestSchema

class FailedToLoadTheProblem(Exception):
    pass

def load_problem(bucket_name: str, problem_id) -> Problem:
    bucket = boto3.resource("s3").Bucket(bucket_name)
    fileobj = io.BytesIO()
    key = f"{problem_id}/tests.json"
    try:
        bucket.download_fileobj(key, fileobj)
    except botocore.exceptions.ClientError as exc:
        raise FailedToLoadTheProblem from exc
    payload = json.loads(fileobj.getvalue())
    tests = TestSchema().load(payload, many=True)
    return Problem(problem_id, tests)

def list_problem_ids(bucket_name: str) -> List[str]:
    bucket = boto3.resource("s3").Bucket(bucket_name)
    return [file.key.split("/")[0] for file in bucket.objects.all()]
