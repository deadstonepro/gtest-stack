from typing import List
from datetime import datetime
from dataclasses import dataclass

from flask_login import UserMixin

from marshmallow_dataclass import class_schema

__all__ = (
    "Solution", "SolutionSchema",
    "Test", "TestSchema",
    "Problem", "ProblemSchema",
    "Report", "ReportSchema",
    "User", "UserSchema")

@dataclass
class Solution:
    solution_id: str
    submitted: datetime
    user_id: str
    problem_id: str
    source_code: str
    language: str

@dataclass
class Report(Solution):
    tests_passed: int
    tests_overall: int

ReportSchema = class_schema(Report)

SolutionSchema = class_schema(Solution)

@dataclass
class Test:
    input_data: str
    output_data: str

TestSchema = class_schema(Test)

@dataclass
class Problem:
    problem_id: str
    tests: List[Test]

ProblemSchema = class_schema(Problem)

@dataclass
class User(UserMixin):
    user_id: str
    login: str
    hashed_password: str

    def get_id(self):
        """ To deal with the flask_login user interface. """
        return self.user_id

UserSchema = class_schema(User)
