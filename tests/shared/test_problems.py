import io
import json
import uuid

import moto
import boto3
import pytest

from shared import problems

@pytest.fixture(scope="function")
def empty_bucket():
    with moto.mock_s3():
        s3 = boto3.resource("s3")
        bucket = s3.Bucket("test-bucket")
        bucket.create()
        yield bucket

@pytest.fixture(scope="function")
def bucket(empty_bucket):
    objects = [
        {
            "key": "problem1/tests.json",
            "tests": [{
                "input_data": "problem1 input data",
                "output_data": "problem1 output data"
            }]
        },
        {
            "key": "problem3/tests.json",
            "tests": [
                {
                    "input_data": "1 2",
                    "output_data": "3"
                },
                {
                    "input_data": "4 5",
                    "output_data": "9"
                }
            ]
        }
    ]
    for obj in objects:
        payload = json.dumps(obj["tests"]).encode()
        fileobj = io.BytesIO(payload)
        empty_bucket.upload_fileobj(fileobj, obj["key"])
    yield empty_bucket

def test_load_problem_single_test(bucket):
    problem = problems.load_problem(bucket.name, "problem1")
    assert problem.problem_id == "problem1"
    assert len(problem.tests) == 1
    assert problem.tests[0].input_data == "problem1 input data"
    assert problem.tests[0].output_data == "problem1 output data"

def test_load_problem_not_found(bucket):
    with pytest.raises(problems.FailedToLoadTheProblem):
        problems.load_problem(bucket.name, "problem2")

def test_list_problem_ids(bucket):
    problem_ids = problems.list_problem_ids(bucket.name)
    assert sorted(problem_ids) == ["problem1", "problem3"]
