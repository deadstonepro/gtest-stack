from gtest_web.app import app

if __name__ == "__main__":
    app.config.update({
        "TESTING": True,
        "TEMPLATES_AUTO_RELOAD": True
    })
    app.run(host="0.0.0.0", port=8080)
