import asyncio
import tempfile
import functools
import dataclasses
from pathlib import Path
from typing import Tuple, List, Callable
from contextlib import contextmanager

from shared import data

def language_to_extension(language: str) -> str:
    if language == "python3":
        return "py"
    raise LookupError(f"Language is not supported: {language}")

@contextmanager
def unpack_source(solution: data.Solution) -> Tuple[Path, Path]:
    with tempfile.TemporaryDirectory() as temp_dirs:
        sourced = Path(temp_dirs)
        sourcef = sourced / f"source.{language_to_extension(solution.language)}"
        with sourcef.open("w") as sources:
            sources.write(solution.source_code)
        yield sourced, sourcef

async def run_shell(command: str, input_data: str) -> Tuple[str, str, int]:
    proc = await asyncio.create_subprocess_shell(
            command,
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate(input_data.encode())
    return stdout.decode(), stderr.decode(), proc.returncode

def execute_python3(sourcef: Path, input_data: str) -> str:
    command = f"python3 {sourcef.resolve()}"
    stdout, _, _ = asyncio.run(run_shell(command, input_data))
    return stdout

def compare_numeric_output(first: str, second: str) -> bool:
    def to_numeric_list(str_list: str) -> List[int]:
        return [int(w) for w in str_list.split()]
    return to_numeric_list(first) == to_numeric_list(second)

def run_tests(runner: Callable[[str], str],
              evaluator: Callable[[str, str], bool],
              tests: List[data.Test]) -> int:
    """ Returns number of tests passed. """
    tests_passed = 0
    for test in tests:
        real_output_data = runner(test.input_data)
        if evaluator(real_output_data, test.output_data):
            tests_passed += 1
    return tests_passed

def evalute_python3_solution(solution: data.Solution, problem: data.Problem) -> data.Report:
    with unpack_source(solution) as (_, sourcef):
        executer = functools.partial(execute_python3, sourcef)
        return data.Report(
            **dataclasses.asdict(solution),
            tests_overall=len(problem.tests),
            tests_passed=run_tests(executer, compare_numeric_output, problem.tests))
