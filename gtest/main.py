import os
import json
import logging
from pathlib import Path
from typing import Optional, Dict
from dataclasses import dataclass

import boto3
import config
import marshmallow

from gtest import core
from shared import data
from shared.problems import load_problem

@dataclass
class Message:
    """ Wrapper over the SQS Message. Exists to unify
    boto3 SQS reponse and AWS Lambda event. """

    message_id: str
    receipt_handle: str
    body: str

    @staticmethod
    def from_boto3_native(msg):
        return Message(
            msg.message_id,
            msg.receipt_handle,
            msg.body)

    @staticmethod
    def from_dict(msg: Dict):
        return Message(
            msg["messageId"],
            msg["receiptHandle"],
            msg["body"])

    def to_entry(self) -> Dict:
        return {
            "Id": self.message_id,
            "ReceiptHandle": self.receipt_handle }

class InvalidMessage(Exception):
    pass

class ProblemNotFound(Exception):
    pass

class GTest():
    _report_table = None
    _solution_queue_url = None

    def _get_config_file(self) -> Path:
        if conff := os.environ.get("GTEST_CONF"):
            return Path(conff)
        conff = Path("gtest.cfg")
        if conff.is_file():
            return conff
        raise FileNotFoundError("Couldn't resolve a config file.")

    def _make_config(self) -> config.Config:
        conff = self._get_config_file()
        with conff.open("r") as confr:
            return config.Config(confr)

    def __init__(self):
        self._config = self._make_config()
        self._logger = logging.getLogger()
        self._logger.setLevel(logging.INFO)
        self._start_log()
        sqs = boto3.resource("sqs")
        dynamodb = boto3.resource("dynamodb")
        self._report_table = dynamodb.Table(self._config["REPORT_TABLE_NAME"])
        self._solution_queue = sqs.Queue(self._config["SOLUTION_QUEUE_URL"])

    def _start_log(self):
        self._logger.info("Starting gtest...")
        config_params = ("report_table", "solution_queue")
        for param in config_params:
            self._logger.info("%s: %s", param, self._config.get(param))
        self._logger.info(os.environ) # Delete me

    def _fetch_sqs_message(self) -> Optional[Message]:
        messages = self._solution_queue.receive_messages(
            MaxNumberOfMessages=1,
            WaitTimeSeconds=1)
        if not messages:
            return None
        return Message.from_boto3_native(messages[0])

    @staticmethod
    def aws_lambda_event_to_message(event) -> Optional[Message]:
        if not (records := event.get("Records")):
            return None
        return Message.from_dict(records[0])

    def _get_solution(self, message) -> data.Solution:
        try:
            solution_item = json.loads(message.body)
        except json.decoder.JSONDecodeError as exception:
            raise InvalidMessage(exception.msg, exception.pos) from exception
        try:
            return data.SolutionSchema().load(solution_item)
        except marshmallow.exceptions.ValidationError as exception:
            raise InvalidMessage(exception.messages) from exception

    def _send_report(self, report: data.Report):
        report_item = data.ReportSchema().dump(report)
        self._report_table.put_item(Item=report_item)

    def _delete_message(self, message):
        self._solution_queue.delete_messages(Entries=[message.to_entry()])

    def _handle_solution(self, solution: data.Solution):
        problem = load_problem(self._config["PROBLEM_BUCKET"], solution.problem_id)
        self._logger.info("Loaded a problem: %s", problem.problem_id)
        report = core.evalute_python3_solution(solution, problem)
        self._logger.info("Generated a report for %s", solution.solution_id)
        self._send_report(report)
        self._logger.info("Sent a report for %s", solution.solution_id)

    def handle_message(self, message: Message):
        self._logger.info("Handling a message: %s", message.message_id)
        try:
            solution = self._get_solution(message)
            self._logger.info("Received a solution: %s", solution.solution_id)
            self._handle_solution(solution)
        except InvalidMessage as exception:
            self._logger.warning("Invalid message: %s", exception.args)
        except ProblemNotFound as exception:
            self._logger.warning("Problem not found: %s", exception.args)
        finally:
            self._delete_message(message)
            self._logger.info("Message deleted: %s", message.message_id)

    def handler_loop(self):
        self._logger.info("Starting the handler loop")
        while True:
            message = self._fetch_sqs_message()
            if not message:
                continue
            self.handle_message(message)

def handler(event, _):
    """ Stands for handling AWS Lambda events. """
    app = GTest()
    message = GTest.aws_lambda_event_to_message(event)
    if not message:
        return
    app.handle_message(message)
